list () {
  brew list
}

install () {
  missing_from "$(list)"
  pack_section homebrew recipes | brew install
}

upgrade_cask () {
  echo "      Upgrading cask: $1..."
  brew cask uninstall --force "$1"
  rm -rf "/opt/homebrew-cask/Caskroom/$1"
  brew cask install "$1"
}

upgrade_casks () {
  # update brew casks -- eventually this should not be necessary
  # (https://github.com/caskroom/homebrew-cask/issues/4678)
  staging_location="$(brew cask doctor | awk 'f{print;f=0} /==> Homebrew-Cask Staging Location:/{f=1}')"
  table_divider 3
  table_row "cask" "installed version" "current version"
  table_divider 3
  for cask in $(brew cask list); do
    info="$(brew cask info $cask)"
    current_version="$(echo "$info" | awk 'NR==1 {print $2}')"
    installed_version="$(echo "$info" | awk -F '[/ ]' 'NR==3 {print $6}')"
    table_row $col_length "$cask" "$installed_version" "$current_version"
    if [ "$installed_version" != "$current_version" ]; then
      upgrade_cask "$cask"
    elif [ "$current_version" = "latest" ]; then
      if [ "$1" = "-a" ]; then
        upgrade_cask "$cask"
      else
        echo "Not upgrading $cask (to upgrade, run with '-a')"
      fi
    fi
  done
}

update () {
  if installed brew; then
    echo "Updating homebrew..."
    cd "$(brew --repo)"
    git prune
    git gc
    cd - > /dev/null
    brew update
    brew upgrade
    brew cleanup
    brew prune
    brew doctor
    echo "Upgrading homebrew casks..."
    upgrade_casks "$@"
    brew cask cleanup
  else
    echo "Hombrew is not installed, skipping"
  fi
}
